"use strict";

let arr = [
  1234567890123456789012345678901234567890n,
  "hello",
  "world",
  23,
  "23",
  null,
  false,
];

let arrType = "string";

function filterBy(array, type) {
  return array.filter((e) => typeof e !== type);
}

const filteredArray = filterBy(arr, arrType);
console.log(filteredArray);
